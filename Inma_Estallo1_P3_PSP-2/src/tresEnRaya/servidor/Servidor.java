package tresEnRaya.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {
    private boolean turno;
    private int[][] fichas;
    private int turnos;
    private ArrayList<Socket> clientes;
    private int puerto;

    //Conector del servidor al que se le pasa el parametro  puerto
    public Servidor(int puerto) throws IOException {
        turno = true;
        fichas = new int[3][3];
        turnos = 1;
        clientes = new ArrayList<Socket>();
        this.puerto = puerto;
        conectar();
    }

    //Metodo que conecta con el socket
    public void conectar() throws IOException {
        int contador = 0;
        for(int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                fichas[i][j] = contador;
                contador++;
            }
        }
        ServerSocket socketServidor = new ServerSocket(puerto);
        System.out.println("Tres en raya");

        while(true){
            Socket socketCliente = socketServidor.accept();
            clientes.add(socketCliente);
            Hilo hilo = new Hilo(socketCliente, clientes, turnos, fichas);
            hilo.start();
            turnos++;
        }
    }

    //Metodo main para ejecutar el servidor
    public static void main(String[] args) throws IOException {
        new Servidor(4444);
    }
}
