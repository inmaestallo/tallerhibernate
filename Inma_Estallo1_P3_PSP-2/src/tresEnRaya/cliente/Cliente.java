package tresEnRaya.cliente;



import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Cliente implements ActionListener{
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JTextField txtJugador;
    private JPanel panel;


    private String localhost;
    private int puerto;
    private Socket socket;
    private DataInputStream entrada;
    private DataOutputStream salida;
    private JButton[][] botones;
    private String mensaje;
    private boolean turno;

    //Constructor  al que se   le pasa el host  el puerto y los botones
    public Cliente(String localhost, int puerto) throws IOException {
        this.localhost = localhost;
        this.puerto = puerto;
        botones = new JButton[3][3];
        //Se asignan los botones a la matriz de botones
        botones[0][0] = button1;
        botones[0][1] = button2;
        botones[0][2] = button3;
        botones[1][0] = button4;
        botones[1][1] = button5;
        botones[1][2] = button6;
        botones[2][0] = button7;
        botones[2][1] = button8;
        botones[2][2] = button9;
        addActionListener(this);
        socket = new Socket(localhost, puerto);
        entrada = new DataInputStream(socket.getInputStream());
        salida = new DataOutputStream(socket.getOutputStream());
        init();
        conectar();
    }

    //Metodo que inicia la vista
    public void init(){
        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setVisible(true);
    }

    //Metodo en el que se le asigna a los botones un ActionListener y se les da un  tamaño
    public void addActionListener(ActionListener listener){
        button1.addActionListener(listener);

        button2.addActionListener(listener);
        button3.addActionListener(listener);
        button4.addActionListener(listener);
        button5.addActionListener(listener);
        button6.addActionListener(listener);
        button7.addActionListener(listener);
        button8.addActionListener(listener);
        button9.addActionListener(listener);

    }

    //Metodo obligatorio del ActionListener donde se le pasa al switch el actionCommand de cada boton
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando) {
            case "Button1":
                comprobarFicha(0, 0);
                break;
            case "Button2":
                comprobarFicha(0, 1);
                break;
            case "Button3":
                comprobarFicha(0, 2);
                break;
            case "Button4":
                comprobarFicha(1, 0);
                break;
            case "Button5":
                comprobarFicha(1,1);
                break;
            case "Button6":
                comprobarFicha(1, 2);
                break;
            case "Button7":
                comprobarFicha(2,0);
                break;
            case "Button8":
                comprobarFicha(2, 1);
                break;
            case "Button9":
                comprobarFicha(2, 2);
                break;
        }
    }

    //Metodo que comprueb donde esta la ficha
    public void comprobarFicha(int fila, int columna){
        try{
            if (turno){
                String xy = fila + "," + columna;
                salida.writeUTF(xy);
            }
            else{
                txtJugador.setText("Espere su turno");
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    //Metodo para que se conecten los clientes y servidores
    public void conectar(){
        try{
            mensaje = entrada.readUTF();
            String split[] = mensaje.split(",");
            if (split[1].equalsIgnoreCase("true")){
                txtJugador.setText("Jugador 1: X");
            }
            else{
                txtJugador.setText("Jugador 2: O");
            }
            String partida = split[0];
            turno = Boolean.valueOf(split[1]);
            while (true){
                mensaje = entrada.readUTF();
                String mensajes[] = mensaje.split(",");
                int ficha = Integer.parseInt(mensajes[0]);
                int fila = Integer.parseInt(mensajes[1]);
                int columna = Integer.parseInt(mensajes[2]);
                System.out.println( ficha+" "+fila+" "+columna);

                if (ficha == 1){
                    botones[fila][columna].setText("X");
                }
                else{
                    botones[fila][columna].setText("O");
                }

                botones[fila][columna].removeActionListener(botones[fila][columna].getActionListeners()[0]);
                if (turno == true){
                    turno = false;
                }
                else if(turno == false){
                    turno = true;
                }
                if (mensajes[3].equalsIgnoreCase("Gana el jugador 1")){
                    txtJugador.setText("Gana el jugador 1");
                    for(int i = 0; i < 3; i++){
                        for (int j = 0; j < 3; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
                else if(mensajes[3].equalsIgnoreCase("Gana el jugador 2")){
                    txtJugador.setText("Gana el jugador 2");
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j < 3; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
                else if(mensajes[3].equalsIgnoreCase("empate")){
                    txtJugador.setText("Empate");
                    for (int i = 0; i < 3; i++){
                        for(int j = 0; j < 3; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    //Metodo main para ejecutar los clientes y sus tableros
    public static void main(String[] args) throws IOException {
        new Cliente("localhost", 4444);
    }


















}
