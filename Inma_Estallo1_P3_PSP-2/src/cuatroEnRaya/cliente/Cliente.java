package cuatroEnRaya.cliente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Cliente implements ActionListener {


    private String localhost;
    private int puerto;
    private Socket socket;
    private DataInputStream entrada;
    private DataOutputStream salida;
    private JButton[][] botones;
    private String mensaje;
    private boolean turnoPartida;


    private JPanel panel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JTextField txtJugador;
    private JButton button10;
    private JButton button11;
    private JButton button12;
    private JButton button13;
    private JButton button14;
    private JButton button15;
    private JButton button16;
    private JButton button17;
    private JButton button18;
    private JButton button19;
    private JButton button20;
    private JButton button21;
    private JButton button22;
    private JButton button23;
    private JButton button24;
    private JButton button25;
    private JButton button26;
    private JButton button27;
    private JButton button28;
    private JButton button29;
    private JButton button30;
    private JButton button31;
    private JButton button32;
    private JButton button33;
    private JButton button34;
    private JButton button35;
    private JButton button36;
    private JButton button37;
    private JButton button38;
    private JButton button39;
    private JButton button40;
    private JButton button41;
    private JButton button42;




    public Cliente(String localhost, int puerto) throws IOException {
        this.localhost = localhost;
        this.puerto = puerto;
        botones = new JButton[6][7];

        botones[0][0] = button1;
        botones[0][1] = button2;
        botones[0][2] = button3;
        botones[0][3] = button4;
        botones[0][4] = button5;
        botones[0][5] = button6;
        botones[0][6] = button7;
        botones[1][0] = button8;
        botones[1][1] = button9;
        botones[1][2] = button10;
        botones[1][3] = button11;
        botones[1][4] = button12;
        botones[1][5] = button13;
        botones[1][6] = button14;
        botones[2][0] = button15;
        botones[2][1] = button16;
        botones[2][2] = button17;
        botones[2][3] = button18;
        botones[2][4] = button19;
        botones[2][5] = button20;
        botones[2][6] = button21;
        botones[3][0] = button22;
        botones[3][1] = button23;
        botones[3][2] = button24;
        botones[3][3] = button25;
        botones[3][4] = button26;
        botones[3][5] = button27;
        botones[3][6] = button28;
        botones[4][0] = button29;
        botones[4][1] = button30;
        botones[4][2] = button31;
        botones[4][3] = button32;
        botones[4][4] = button33;
        botones[4][5] = button34;
        botones[4][6] = button35;
        botones[5][0] = button36;
        botones[5][1] = button37;
        botones[5][2] = button38;
        botones[5][3] = button39;
        botones[5][4] = button40;
        botones[5][5] = button41;
        botones[5][6] = button42;

        addActionListener(this);
        socket = new Socket(localhost, puerto);
        entrada = new DataInputStream(socket.getInputStream());
        salida = new DataOutputStream(socket.getOutputStream());
        init();
        conectar();
    }


    //Metodo para asignarles a los botones un ActionListener y darles un tamaño
    public void addActionListener(ActionListener listener){
        button1.addActionListener(listener);

        button2.addActionListener(listener);

        button3.addActionListener(listener);

        button4.addActionListener(listener);

        button5.addActionListener(listener);

        button6.addActionListener(listener);

        button7.addActionListener(listener);

        button8.addActionListener(listener);

        button9.addActionListener(listener);

        button10.addActionListener(listener);

        button11.addActionListener(listener);

        button12.addActionListener(listener);

        button13.addActionListener(listener);

        button14.addActionListener(listener);

        button15.addActionListener(listener);

        button16.addActionListener(listener);

        button17.addActionListener(listener);

        button18.addActionListener(listener);

        button19.addActionListener(listener);

        button20.addActionListener(listener);

        button21.addActionListener(listener);

        button22.addActionListener(listener);

        button23.addActionListener(listener);

        button24.addActionListener(listener);

        button25.addActionListener(listener);

        button26.addActionListener(listener);

        button27.addActionListener(listener);

        button28.addActionListener(listener);

        button29.addActionListener(listener);

        button30.addActionListener(listener);

        button31.addActionListener(listener);

        button32.addActionListener(listener);

        button33.addActionListener(listener);

        button34.addActionListener(listener);

        button35.addActionListener(listener);

        button36.addActionListener(listener);

        button37.addActionListener(listener);

        button38.addActionListener(listener);

        button39.addActionListener(listener);

        button40.addActionListener(listener);

        button41.addActionListener(listener);

        button42.addActionListener(listener);

    }
    //Metodo para iniciar la vista y darle diferentes propiedades
    public void init(){
        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setVisible(true);
    }


    //Metodo para que se conecten los clientes y servidores
    public void conectar(){
        try{
            mensaje = entrada.readUTF();
            String split[] = mensaje.split(",");
            if (split[1].equalsIgnoreCase("true")){
                txtJugador.setText("Jugador 1: X");
            }
            else{
                txtJugador.setText("Jugador 2: O");
            }
            String partida = split[0];
            turnoPartida = Boolean.valueOf(split[1]);
            while (true){
                mensaje = entrada.readUTF();
                String mensajes[] = mensaje.split(",");
                int ficha = Integer.parseInt(mensajes[0]);
                int fila = Integer.parseInt(mensajes[1]);
                int columna = Integer.parseInt(mensajes[2]);

                if (ficha == 1){
                    botones[fila][columna].setText("X");
                }
                else{
                    botones[fila][columna].setText("O");
                }
                //Se les borra el actionlistener temporalmente para controlar los turnos
                botones[fila][columna].removeActionListener(botones[fila][columna].getActionListeners()[0]);
                if (turnoPartida == true){
                    turnoPartida = false;
                }
                else if(turnoPartida == false){
                    turnoPartida = true;
                }
                if (mensajes[3].equalsIgnoreCase("Gana jugador 1")){
                    txtJugador.setText("Gana  jugador 1");
                    for(int i = 0; i < 6; i++){
                        for (int j = 0; j < 7; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
                else if(mensajes[3].equalsIgnoreCase("Gana  jugador 2")){
                    txtJugador.setText("Gana jugador 2");
                    for (int i = 0; i < 6; i++){
                        for (int j = 0; j < 7; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
                else if(mensajes[3].equalsIgnoreCase("empate")){
                    txtJugador.setText("Empate");
                    for (int i = 0; i < 6; i++){
                        for(int j = 0; j < 7; j++){
                            botones[i][j].removeActionListener(botones[i][j].getActionListeners()[0]);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Metodo ficha que se le pasa fila y columna para comprobar en que casilla esta la ficha
    public void comprobarFicha(int fila, int columna){
        try{
            if (turnoPartida){
                String xy = fila + "," + columna + ",";
                salida.writeUTF(xy);
            }
            else{
                txtJugador.setText("Espere su turno");
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    //Metodo obligatorio del ActionListener donde se le pasa al switch el actionCommand de cada boton
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando) {
            case "Button1":
                comprobarFicha(0, 0);
                break;
            case "Button2":
                comprobarFicha(0, 1);
                break;
            case "Button3":
                comprobarFicha(0, 2);
                break;
            case "Button4":
                comprobarFicha(0, 3);
                break;
            case "Button5":
                comprobarFicha(0,4);
                break;
            case "Button6":
                comprobarFicha(0, 5);
                break;
            case "Button7":
                comprobarFicha(0,6);
                break;
            case "Button8":
                comprobarFicha(1, 0);
                break;
            case "Button9":
                comprobarFicha(1, 1);
                break;
            case "Button10":
                comprobarFicha(1, 2);
                break;
            case "Button11":
                comprobarFicha(1, 3);
                break;
            case "Button12":
                comprobarFicha(1, 4);
                break;
            case "Button13":
                comprobarFicha(1, 5);
                break;
            case "Button14":
                comprobarFicha(1, 6);
                break;
            case "Button15":
                comprobarFicha(2, 0);
                break;
            case "Button16":
                comprobarFicha(2, 1);
                break;
            case "Button17":
                comprobarFicha(2, 2);
                break;
            case "Button18":
                comprobarFicha(2, 3);
                break;
            case "Button19":
                comprobarFicha(2, 4);
                break;
            case "Button20":
                comprobarFicha(2, 5);
                break;
            case "Button21":
                comprobarFicha(2, 6);
                break;
            case "Button22":
                comprobarFicha(3, 0);
                break;
            case "Button23":
                comprobarFicha(3, 1);
                break;
            case "Button24":
                comprobarFicha(3, 2);
                break;
            case "Button25":
                comprobarFicha(3, 3);
                break;
            case "Button26":
                comprobarFicha(3, 4);
                break;
            case "Button27":
                comprobarFicha(3, 5);
                break;
            case "Button28":
                comprobarFicha(3, 6);
                break;
            case "Button29":
                comprobarFicha(4, 0);
                break;
            case "Button30":
                comprobarFicha(4, 1);
                break;
            case "Button31":
                comprobarFicha(4, 2);
                break;
            case "Button32":
                comprobarFicha(4, 3);
                break;
            case "Button33":
                comprobarFicha(4, 4);
                break;
            case "Button34":
                comprobarFicha(4, 5);
                break;
            case "Button35":
                comprobarFicha(4, 6);
                break;
            case "Button36":
                comprobarFicha(5, 0);
                break;
            case "Button37":
                comprobarFicha(5, 1);
                break;
            case "Button38":
                comprobarFicha(5, 2);
                break;
            case "Button39":
                comprobarFicha(5, 3);
                break;
            case "Button40":
                comprobarFicha(5, 4);
                break;
            case "Button41":
                comprobarFicha(5, 5);
                break;
            case "Button42":
                comprobarFicha(5, 6);
                break;

        }
    }

    //Metodo main para ejecutar los clientes y sus tableros
    public static void main(String[] args) throws IOException {
        new Cliente("localhost", 4444);
    }
}
