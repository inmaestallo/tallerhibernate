package cuatroEnRaya.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {
    private boolean turno;
    private int[][] fichas;
    private int turnos;
    private ArrayList<Socket> clientes;
    private int puerto;

    //Constructor del servidor al que se le pasa por parámetros el puerto
    public Servidor(int puerto) throws IOException {
        turno = true;
        fichas = new int[6][7];
        turnos = 1;
        clientes = new ArrayList<Socket>();
        this.puerto = puerto;
        conectar();
    }

    //Metodo conectar que conecta
    public void conectar() throws IOException {
        int contador = 0;
        for(int i = 0; i < 6; i++){
            for (int j = 0; j < 7; j++){
                fichas[i][j] = contador;
                contador++;
            }
        }
        ServerSocket socketServidor = new ServerSocket(puerto);
        System.out.println("Cuatro en raya");

        while(true){
            Socket socketCliente = socketServidor.accept();
            clientes.add(socketCliente);
            Hilo hilo = new Hilo(socketCliente, clientes, turnos, fichas);
            hilo.start();
            turnos++;
        }
    }

    //Metodo main para ejecutar el servidor
    public static void main(String[] args) throws IOException {
        new Servidor(4444);
    }
}
