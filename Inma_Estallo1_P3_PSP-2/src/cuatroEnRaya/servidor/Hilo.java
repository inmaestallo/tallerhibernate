package cuatroEnRaya.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Hilo extends Thread{
    private Socket socketCliente;
    private DataOutputStream salida;
    private DataInputStream entrada;
    private int turnoJugador;
    private int[][] fichas;
    private boolean turno;
    private ArrayList<Socket> clientes;
    private int contador;

    //Constructor al que se le pasa el socketCliente, los clientes, el turnoJugador,fichas y el contador
    public Hilo(Socket socketCliente, ArrayList<Socket> clientes, int turnoJugador, int[][] fichas){
        this.socketCliente = socketCliente;
        this.clientes = clientes;
        this.turnoJugador = turnoJugador;
        this.fichas = fichas;
        this.contador = 0;
    }
    //Metodo obligatorio que ejecuta el hilo
    @Override
    public void run(){
        try{
            entrada = new DataInputStream(socketCliente.getInputStream());
            salida = new DataOutputStream(socketCliente.getOutputStream());
            if (turnoJugador == 1){
                String mensaje = "Tu turno,true";
                salida.writeUTF(mensaje);
            }
            else{
                String mensaje = "Turno del otro,false";
                salida.writeUTF(mensaje);
            }

            while(true){
                String input = entrada.readUTF();
                String[] entradas = input.split(",");
                int fila = Integer.parseInt(entradas[0]);
                int columna = Integer.parseInt(entradas[1]);
                if(turnoJugador == 1){
                    fichas[fila][columna] = 1;
                }
                else{
                    fichas[fila][columna] = 2;
                }
                contador++;
                String salida =  + turnoJugador + "," + fila + "," + columna + ",";
                String ganador = comprobarGanador();
                if (ganador.equalsIgnoreCase("jugador1")){
                    salida = salida + "Gana jugador 1";
                }
                else if (ganador.equalsIgnoreCase("jugador2")){
                    salida = salida + "Gana jugador 2";
                }
                else if (ganador.equalsIgnoreCase("ninguno")){
                    salida = salida + " ";
                }

                for (Socket socketCliente : clientes){
                    this.salida = new DataOutputStream(socketCliente.getOutputStream());
                    this.salida.writeUTF(salida);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Metodo que comprueba el ganador
    private String comprobarGanador() {


      if(fichas[0][0] == 1 && fichas[1][1] == 1 && fichas[2][2] == 1 && fichas[3][3] == 1){
          return "jugador1";
      }
       else if(fichas[0][0] == 2 && fichas[1][1] == 2 && fichas[2][2] == 2 && fichas[3][3] == 2){
            return "jugador2";
        }
       else if (fichas[1][1] == 1 && fichas[2][2] == 1 && fichas[3][3] == 1 && fichas[4][4] == 1){
          return "jugador1";
      }
      else if (fichas[1][1] == 2 && fichas[2][2] == 2 && fichas[3][3] == 2 && fichas[4][4] == 2){
          return "jugador2";
      }
        else if(fichas[2][2] == 1 && fichas[3][3] == 1 && fichas[4][4] == 1 && fichas[5][5] == 1) {
            return "jugador1";
      }
      else if(fichas[2][2] == 2 && fichas[3][3] == 2 && fichas[4][4] == 2 && fichas[5][5] == 2) {
          return "jugador2";
      }
     else if(fichas[1][0] == 1 && fichas[2][1] == 1 && fichas[3][2] == 1 && fichas[4][3] == 1){
          return "jugador1";
      }
      else if(fichas[1][0] == 2 && fichas[2][1] == 2 && fichas[3][2] == 2 && fichas[4][3] == 2){
          return "jugador2";
      }
        else if(fichas[2][1] == 1 && fichas[3][2] == 1 && fichas[4][3] == 1 && fichas[5][4] == 1) {
          return "jugador1";
      }
      else if(fichas[2][1] == 2 && fichas[3][2] == 2 && fichas[4][3] == 2 && fichas[5][4] == 2) {
          return "jugador2";
      }
      else if(fichas[2][0] == 1 && fichas[3][1] == 1 && fichas[4][2] == 1 && fichas[5][3] == 1) {
          return "jugador1";
      }
      else if(fichas[2][0] == 2 && fichas[3][1] == 2 && fichas[4][2] == 2 && fichas[5][3] == 2) {
          return "jugador2";
      }
      else if(fichas[0][1] == 1 && fichas[1][2] == 1 && fichas[2][3] == 1 && fichas[3][4] == 1){
          return "jugador1";
      }
      else if(fichas[0][1] == 2 && fichas[1][2] == 2 && fichas[2][3] == 2 && fichas[3][4] == 2){
          return "jugador2";
      }
      else if(fichas[1][2] == 1 && fichas[2][3] == 1 && fichas[3][4] == 1 && fichas[4][5] == 1){
          return "jugador1";
      }
      else if(fichas[1][2] == 2 && fichas[2][3] == 2 && fichas[3][4] == 2 && fichas[4][5] == 2){
          return "jugador2";
      }
      else if(fichas[2][3] == 1 && fichas[3][4] == 1 && fichas[4][5] == 1 && fichas[5][6] == 1){
          return "jugador1";
      }
      else if(fichas[2][3] == 2 && fichas[3][4] == 2 && fichas[4][5] == 2 && fichas[5][6] == 2){
          return "jugador2";
      }
       else if(fichas[0][2] == 1 && fichas[1][3] == 1 && fichas[2][4] == 1 && fichas[3][5] == 1){
          return "jugador1";
      }
      else if(fichas[0][2] == 2 && fichas[1][3] == 2 && fichas[2][4] == 2 && fichas[3][5] == 2){
          return "jugador2";
      }
      else if (fichas[1][3] == 1 && fichas[2][4] == 1 && fichas[3][5] == 1 && fichas[4][6] == 1){
          return "jugador1";
      }
      else if (fichas[1][3] == 2 && fichas[2][4] == 2 && fichas[3][5] == 2 && fichas[4][6] == 2){
          return "jugador2";
      }
       else if(fichas[0][3] == 1 && fichas[1][4] == 1 && fichas[2][5] == 1 && fichas[3][6] == 1) {
          return "jugador1";
      }
      else if(fichas[0][3] == 2 && fichas[1][4] == 2 && fichas[2][5] == 2 && fichas[3][6] == 2) {
          return "jugador2";
      }
      else if(fichas[0][6] == 1 && fichas[1][5] == 1 && fichas[2][4] == 1 && fichas[3][3] == 1){
          return "jugador1";
      }
      else if(fichas[0][6] == 2 && fichas[1][5] == 2 && fichas[2][4] == 2 && fichas[3][3] == 2){
          return "jugador2";
      }
      else if(fichas[1][5] == 1 && fichas[2][4] == 1 && fichas[3][3] == 1 && fichas[4][2] == 1){
          return "jugador1";
      }
      else if(fichas[1][5] == 2 && fichas[2][4] == 2 && fichas[3][3] == 2 && fichas[4][2] == 2){
          return "jugador2";
      }
      else if(fichas[2][4] == 1 && fichas[3][3] == 1 && fichas[4][2] == 1 && fichas[5][1] == 1){
          return "jugador1";
      }
      else if(fichas[2][4] == 2 && fichas[3][3] == 2 && fichas[4][2] == 2 && fichas[5][1] == 2){
          return "jugador2";
      }
      else if(fichas[0][5] == 1 && fichas[1][4] == 1 && fichas[2][3] == 1 && fichas[3][2] == 1){
          return "jugador1";
      }
      else if(fichas[0][5] == 2 && fichas[1][4] == 2 && fichas[2][3] == 2 && fichas[3][2] == 2){
          return "jugador2";
      }
       else if(fichas[1][4] == 1 && fichas[2][3] == 1 && fichas[3][2] == 1 && fichas[4][1] == 1){
          return "jugador1";
      }
      else if(fichas[1][4] == 2 && fichas[2][3] == 2 && fichas[3][2] == 2 && fichas[4][1] == 2){
          return "jugador2";
      }
       else if(fichas[2][3] == 1 && fichas[3][2] == 1 && fichas[4][1] == 1 && fichas[5][0] == 1){
          return "jugador1";
      }
      else if(fichas[2][3] == 2 && fichas[3][2] == 2 && fichas[4][1] == 2 && fichas[5][0] == 2){
          return "jugador2";
      }
       else if(fichas[0][4] == 1 && fichas[1][3] == 1 && fichas[2][2] == 1 && fichas[3][1] == 1){
          return "jugador1";
      }
      else if(fichas[0][4] == 2 && fichas[1][3] == 2 && fichas[2][2] == 2 && fichas[3][1] == 2){
          return "jugador2";
      }
       else if(fichas[1][3] == 1 && fichas[2][2] == 1 && fichas[3][1] == 1 && fichas[4][0] == 1){
           return "jugador1";
      }
      else if(fichas[1][3] == 2 && fichas[2][2] == 2 && fichas[3][1] == 2 && fichas[4][0] == 2){
          return "jugador2";
      }
       else if(fichas[0][3] == 1 && fichas[1][2] == 1 && fichas[2][1] == 1 && fichas[3][0] == 1){
          return "jugador1";
      }
      else if(fichas[0][3] == 2 && fichas[1][2] == 2 && fichas[2][1] == 2 && fichas[3][0] == 2){
          return "jugador2";
      }
        else if(fichas[1][6] == 1 && fichas[2][5] == 1 && fichas[3][4] == 1 && fichas[4][3] == 1){
          return "jugador1";
      }
      else if(fichas[1][6] == 2 && fichas[2][5] == 2 && fichas[3][4] == 2 && fichas[4][3] == 2){
          return "jugador2";
      }
      else if(fichas[2][5] == 1 && fichas[3][4] == 1 && fichas[4][3] == 1 && fichas[5][2] == 1){
          return "jugador1";
      }
      else if(fichas[2][5] == 2 && fichas[3][4] == 2 && fichas[4][3] == 2 && fichas[5][2] == 2){
          return "jugador2";
      }
      else if(fichas[2][6] == 1 && fichas[3][5] == 1 && fichas[4][4] == 1 && fichas[5][3] == 1){
          return "jugador1";
      }
      else if(fichas[2][6] == 2 && fichas[3][5] == 2 && fichas[4][4] == 2 && fichas[5][3] == 2){
          return "jugador2";
      }
       else if(fichas[0][0] == 1 && fichas[0][1] == 1 && fichas[0][2] == 1 && fichas[0][3] == 1) {
          return "jugador1";
      }
      else if(fichas[0][0] == 2 && fichas[0][1] == 2 && fichas[0][2] == 2 && fichas[0][3] == 2) {
          return "jugador2";
      }
       else if(fichas[0][1] == 1 && fichas[0][2] == 1 && fichas[0][3] == 1 && fichas[0][4] == 1){
          return "jugador1";
      }
      else if(fichas[0][1] == 2 && fichas[0][2] == 2 && fichas[0][3] == 2 && fichas[0][4] == 2){
          return "jugador2";
      }
       else if(fichas[0][2] == 1 && fichas[0][3] == 1 && fichas[0][4] == 1 && fichas[0][5] == 1){
          return "jugador1";
      }
      else if(fichas[0][2] == 2 && fichas[0][3] == 2 && fichas[0][4] == 2 && fichas[0][5] == 2){
          return "jugador2";
      }
       else if(fichas[0][3] == 1 && fichas[0][4] == 1 && fichas[0][5] == 1 && fichas[0][6] == 1){
          return "jugador1";
      }
      else if(fichas[0][3] == 2 && fichas[0][4] == 2 && fichas[0][5] == 2 && fichas[0][6] == 2){
          return "jugador2";
      }
       else if(fichas[1][0] == 1 && fichas[1][1] == 1 && fichas[1][2] == 1 && fichas[1][3] == 1){
          return "jugador1";
      }
      else if(fichas[1][0] == 2 && fichas[1][1] == 2 && fichas[1][2] == 2 && fichas[1][3] == 2){
          return "jugador2";
      }
      else if(fichas[1][1] == 1 && fichas[1][2] == 1 && fichas[1][3] == 1 && fichas[1][4] == 1){
          return "jugador1";
      }
      else if(fichas[1][1] == 2 && fichas[1][2] == 2 && fichas[1][3] == 2 && fichas[1][4] == 2){
          return "jugador2";
      }
     else if(fichas[1][2] == 1 && fichas[1][3] == 1 && fichas[1][4] == 1 && fichas[1][5] == 1){
          return "jugador1";
      }
      else if(fichas[1][2] == 2 && fichas[1][3] == 2 && fichas[1][4] == 2 && fichas[1][5] == 2){
          return "jugador2";
      }
       else if(fichas[1][3] == 1 && fichas[1][4] == 1 && fichas[1][5] == 1 && fichas[1][6] == 1){
          return "jugador1";
      }
      else if(fichas[1][3] == 2 && fichas[1][4] == 2 && fichas[1][5] == 2 && fichas[1][6] == 2){
          return "jugador2";
      }
       else if(fichas[2][0] == 1 && fichas[2][1] == 1 && fichas[2][2] == 1 && fichas[2][3] == 1){
          return "jugador1";
      }
      else if(fichas[2][0] == 2 && fichas[2][1] == 2 && fichas[2][2] == 2 && fichas[2][3] == 2){
          return "jugador2";
      }
        else if(fichas[2][1] == 1 && fichas[2][2] == 1 && fichas[2][3] == 1 && fichas[2][4] == 1){
          return "jugador1";
      }
      else if(fichas[2][1] == 2 && fichas[2][2] ==2 && fichas[2][3] == 2 && fichas[2][4] == 2){
          return "jugador2";
      }
       else if(fichas[2][2] == 1 && fichas[2][3] == 1 && fichas[2][4] == 1 && fichas[2][5] == 1){
          return "jugador1";
      }
      else if(fichas[2][2] == 2 && fichas[2][3] == 2 && fichas[2][4] == 2 && fichas[2][5] == 2){
          return "jugador2";
      }
       else if(fichas[2][3] == 1 && fichas[2][4] == 1 && fichas[2][5] == 1 && fichas[2][6] == 1){
          return "jugador1";
      }
      else if(fichas[2][3] == 2 && fichas[2][4] == 2 && fichas[2][5] == 2 && fichas[2][6] == 2){
          return "jugador2";
      }
        else if(fichas[3][0] == 1 && fichas[3][1] == 1 && fichas[3][2] == 1 && fichas[3][3] == 1){
          return "jugador1";
      }
      else if(fichas[3][0] == 2 && fichas[3][1] == 2 && fichas[3][2] == 2 && fichas[3][3] == 2){
          return "jugador2";
      }
       else if(fichas[3][1] == 1 && fichas[3][2] == 1 && fichas[3][3] == 1 && fichas[3][4] == 1){
          return "jugador1";
      }
      else if(fichas[3][1] == 2 && fichas[3][2] == 2 && fichas[3][3] == 2 && fichas[3][4] == 2){
          return "jugador2";
      }
       else if(fichas[3][2] == 1 && fichas[3][3] == 1 && fichas[3][4] == 1 && fichas[3][5] == 1){
          return "jugador1";
      }
      else if(fichas[3][2] == 2 && fichas[3][3] == 2 && fichas[3][4] == 2 && fichas[3][5] == 2){
          return "jugador2";
      }
      else if(fichas[3][3] == 1 && fichas[3][4] == 1 && fichas[3][5] == 1 && fichas[3][6] == 1){
          return "jugador1";
      }
      else if(fichas[3][3] == 2 && fichas[3][4] == 2 && fichas[3][5] == 2 && fichas[3][6] == 2){
          return "jugador2";
      }
       else if(fichas[4][0] == 1 && fichas[4][1] == 1 && fichas[4][2] == 1 && fichas[4][3] == 1){
          return "jugador1";
      }
      else if(fichas[4][0] == 2 && fichas[4][1] == 2 && fichas[4][2] == 2 && fichas[4][3] == 2){
          return "jugador2";
      }
      else if(fichas[4][1] == 1 && fichas[4][2] == 1 && fichas[4][3] == 1 && fichas[4][4] == 1){
          return "jugador1";
      }
      else if(fichas[4][1] == 2 && fichas[4][2] == 2 && fichas[4][3] == 2 && fichas[4][4] == 2){
          return "jugador2";
      }
       else if(fichas[4][2] == 1 && fichas[4][3] == 1 && fichas[4][4] == 1 && fichas[4][5] == 1){
          return "jugador1";
      }
      else if(fichas[4][2] == 2 && fichas[4][3] == 2 && fichas[4][4] == 2 && fichas[4][5] == 2){
          return "jugador2";
      }
       else if(fichas[4][3] == 1 && fichas[4][4] == 1 && fichas[4][5] == 1 && fichas[4][6] == 1){
          return "jugador1";
      }
      else if(fichas[4][3] == 2 && fichas[4][4] == 2 && fichas[4][5] == 2 && fichas[4][6] == 2){
          return "jugador2";
      }
        else if(fichas[5][0] == 1 && fichas[5][1] == 1 && fichas[5][2] == 1 && fichas[5][3] == 1){
          return "jugador1";
      }
      else if(fichas[5][0] == 2 && fichas[5][1] == 2 && fichas[5][2] == 2 && fichas[5][3] == 2){
          return "jugador2";
      }
      else if(fichas[5][1] == 1 && fichas[5][2] == 1 && fichas[5][3] == 1 && fichas[5][4] == 1){
          return "jugador1";
      }
      else if(fichas[5][1] == 2 && fichas[5][2] == 2 && fichas[5][3] == 2 && fichas[5][4] == 2){
          return "jugador2";
      }
       else if(fichas[5][2] == 1 && fichas[5][3] == 1 && fichas[5][4] == 1 && fichas[5][5] == 1){
          return "jugador1";
    }
      else if(fichas[5][2] == 2 && fichas[5][3] == 2 && fichas[5][4] == 2 && fichas[5][5] == 2){
          return "jugador2";
      }
       else if(fichas[5][3] == 1 && fichas[5][4] == 1 && fichas[5][5] == 1 && fichas[5][6] == 1){
          return "jugador1";
      }
      else if(fichas[5][3] == 2 && fichas[5][4] == 2 && fichas[5][5] == 2 && fichas[5][6] == 2){
          return "jugador2";
      }
        else if(fichas[0][0] == 1 && fichas[1][0] == 1 && fichas[2][0] == 1 && fichas[3][0] == 1){
          return "jugador1";
      }
      else if(fichas[0][0] == 2 && fichas[1][0] == 2 && fichas[2][0] == 2 && fichas[3][0] == 2){
          return "jugador2";
      }
       else if(fichas[1][0] == 1 && fichas[2][0] == 1 && fichas[3][0] == 1 && fichas[4][0] == 1){
          return "jugador1";
      }
      else if(fichas[1][0] == 2 && fichas[2][0] == 2 && fichas[3][0] == 2 && fichas[4][0] == 2){
          return "jugador2";
      }
        else if(fichas[2][0] == 1 && fichas[3][0] == 1 && fichas[4][0] == 1 && fichas[5][0] == 1){
          return "jugador1";
      }
      else if(fichas[2][0] == 2 && fichas[3][0] == 2 && fichas[4][0] == 2 && fichas[5][0] == 2){
          return "jugador2";
      }
        else if(fichas[0][1] == 1 && fichas[1][1] == 1 && fichas[2][1] == 1 && fichas[3][1] == 1){
          return "jugador1";
      }
      else if(fichas[0][1] == 2 && fichas[1][1] == 2 && fichas[2][1] == 2 && fichas[3][1] == 2){
          return "jugador2";
      }
       else if(fichas[1][1] == 1 && fichas[2][1] == 1 && fichas[3][1] == 1 && fichas[4][1] == 1){
          return "jugador1";
      }
      else if(fichas[1][1] == 2 && fichas[2][1] == 2 && fichas[3][1] == 2 && fichas[4][1] == 2){
          return "jugador2";
      }
       else if(fichas[2][1] == 1 && fichas[3][1] == 1 && fichas[4][1] == 1 && fichas[5][1] == 1){
          return "jugador1";
      }
      else if(fichas[2][1] == 2 && fichas[3][1] == 2 && fichas[4][1] == 2 && fichas[5][1] == 2){
          return "jugador2";
      }
       else if (fichas[0][2] == 1 && fichas[1][2] == 1 && fichas[2][2] == 1 && fichas[3][2] == 1){
          return "jugador1";
      }
      else if (fichas[0][2] == 2 && fichas[1][2] == 2 && fichas[2][2] == 2 && fichas[3][2] == 2){
          return "jugador2";
      }
       else if(fichas[1][2] == 1 && fichas[2][2] == 1 && fichas[3][2] == 1 && fichas[4][2] == 1){
          return "jugador1";
      }
      else if(fichas[1][2] == 2 && fichas[2][2] == 2 && fichas[3][2] == 2 && fichas[4][2] == 2){
          return "jugador2";
      }
        else if(fichas[2][2] == 1 && fichas[3][2] == 1 && fichas[4][2] == 1 && fichas[5][2] == 1){
          return "jugador1";
      }
      else if(fichas[2][2] == 2 && fichas[3][2] == 2 && fichas[4][2] == 2 && fichas[5][2] == 2){
          return "jugador2";
      }
          else if(fichas[0][3] == 1 && fichas[1][3] == 1 && fichas[2][3] == 1 && fichas[3][3] == 1){
          return "jugador1";
      }
      else if(fichas[0][3] == 2 && fichas[1][3] == 2 && fichas[2][3] == 2 && fichas[3][3] == 2){
          return "jugador2";
      }
        else if(fichas[1][3] == 1 && fichas[2][3] == 1 && fichas[3][3] == 1 && fichas[4][3] == 1){
          return "jugador1";
      }
      else if(fichas[1][3] == 2 && fichas[2][3] == 2 && fichas[3][3] == 2 && fichas[4][3] == 2){
          return "jugador2";
      }
      else if(fichas[2][3] == 1 && fichas[3][3] == 1 && fichas[4][3] == 1 && fichas[5][3] == 1){
          return "jugador1";
      }
      else if(fichas[2][3] == 2 && fichas[3][3] == 2 && fichas[4][3] == 2 && fichas[5][3] == 2){
          return "jugador2";
      }
        else if(fichas[0][4] == 1 && fichas[1][4] == 1 && fichas[2][4] == 1 && fichas[3][4] == 1){
          return "jugador1";
      }
      else if(fichas[0][4] == 2 && fichas[1][4] == 2 && fichas[2][4] == 2 && fichas[3][4] == 2){
          return "jugador2";
      }
       else if(fichas[1][4] == 1 && fichas[2][4] == 1 && fichas[3][4] == 1 && fichas[4][4] == 1){
          return "jugador1";
      }
      else if(fichas[1][4] == 2 && fichas[2][4] == 2 && fichas[3][4] == 2 && fichas[4][4] == 2){
          return "jugador2";
      }
       else if (fichas[2][4] == 1 && fichas[3][4] == 1 && fichas[4][4] == 1 && fichas[5][4] == 1){
          return "jugador1";
      }
      else if (fichas[2][4] == 2 && fichas[3][4] == 2 && fichas[4][4] == 2 && fichas[5][4] == 2){
          return "jugador2";
      }
      else if(fichas[0][5] == 1 && fichas[1][5] == 1 && fichas[2][5] == 1 && fichas[3][5] == 1){
          return "jugador1";
      }
      else if(fichas[0][5] == 2 && fichas[1][5] == 2 && fichas[2][5] == 2 && fichas[3][5] == 2){
          return "jugador2";
      }
       else if(fichas[1][5] == 1 && fichas[2][5] == 1 && fichas[3][5] == 1 && fichas[4][5] == 1){
          return "jugador1";
      }
      else if(fichas[1][5] == 2 && fichas[2][5] == 2 && fichas[3][5] == 2 && fichas[4][5] == 2){
          return "jugador2";
      }
        else if(fichas[2][5] == 1 && fichas[3][5] == 1 && fichas[4][5] == 1 && fichas[5][5] == 1){
          return "jugador1";
      }
      else if(fichas[2][5] == 2 && fichas[3][5] == 2 && fichas[4][5] == 2 && fichas[5][5] == 2){
          return "jugador2";
      }
      else if(fichas[0][6] == 1 && fichas[1][6] == 1 && fichas[2][6] == 1 && fichas[3][6] == 1){
          return "jugador1";
      }
      else if(fichas[0][6] == 2 && fichas[1][6] == 2 && fichas[2][6] == 2 && fichas[3][6] == 2){
          return "jugador2";
      }
     else if(fichas[1][6] == 1 && fichas[2][6] == 1 && fichas[3][6] == 1 && fichas[4][6] == 1){
          return "jugador1";
      }
      else if(fichas[1][6] == 2 && fichas[2][6] == 2 && fichas[3][6] == 2 && fichas[4][6] == 2){
          return "jugador2";
      }
       else if(fichas[2][6] == 1 && fichas[3][6] == 1 && fichas[4][6] == 1&& fichas[5][6] == 1){
          return "jugador1";
      }
      else if(fichas[2][6] == 2 && fichas[3][6] == 2 && fichas[4][6] == 2 && fichas[5][6] == 2){
          return "jugador2";
      }
        else if(contador == 42){
            return "empate";
        }
        else{
            return "ninguno";
        }
    }
}
