package datosClimaticos;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

    private ServerSocket socketServidor;
    private int puerto;

    /**
     * Constructor de la clase Servidor. Esta clase inicia un servidor trivial que espera una conexión
     * de una aplicacion cliente por socket. Una vez recibida la conexion de un cliente el servidor
     * escribirá un código, el servidor lo interpretará y devolverá la información
     * que el cliente ha solicitado o un aviso en caso de error
     *
     * @param puerto int que representa el puerto por el que se realizará la conexión a través de socket.
     */
    public Servidor(int puerto) throws IOException {
        this.puerto = puerto;
        //Se inicia el servidor
        arrancarServidor();
    }


    /**
     * Método privado que arranca de manera automatica el servidor. Esperará que se realice la
     * conexión de una aplicación cliente
     */
    private void arrancarServidor()  {


        //Se crea el ServerSocket
        try {
            socketServidor = new ServerSocket(this.puerto);



                /*  Se crea el socket de conexión del cliente para trabajar con él.
                    En un caso más real se utilizaría para crear los canales de
                    comunicacion de entrada y salida con la aplicación cliente
                */
            Socket cienteConectado = socketServidor.accept();

            //Declaramos un PrintStream para escribir datos
            PrintStream salida = new PrintStream(cienteConectado.getOutputStream());

            // Creamos un String recibido para cuando recibamos la orden del cliente
            String recibido = "";

            //Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(cienteConectado.getInputStream());

            while (true) {
                recibido = entrada.readLine();
                System.out.println("Mensaje Recibido: " + recibido);


                switch (recibido) {
                    //En caso de mayor cantidad Suiza
                    case "Mayor cantidad Suiza":
                        /* Se llama el método que escribe mayor cantidad  de Co2 en Suiza*/
                        mayorSuiza(salida);
                        break;
                    //En caso de  mayor cantidad Suiza
                    case "Mayor Cantidad global":
                        /* Se llama el método que escribe mayor cantidad  */
                        mayorGlobal(salida);
                        break;
                    //En caso de ser media suiza
                    case "media suiza":

                        mediaSuiza(salida);
                        /* Se llama el método que escribe la media de suiza  */
                        break;


                    //En caso de ser media global
                    case "media global":

                        mediaGlobal(salida);
                        /* Se llama el método que escribe la media global  */
                        break;
                    case "aumento suiza":
                        /* Se llama el método que escribe aumento suiza */
                        aumentoSuiza(salida);
                        break;
                    case "aumentoglobal":
                        /* Se llama el método que escribe aumento global */
                        aumentoGlobal(salida);
                        break;
                    case "aumentocienaniosSuiza":
                        /* Se llama el método que escribe aumentocienaniosSuiza */
                        aumentocienaSuiza(salida);
                        break;
                    case "aumentocienanios":
                        /* Se llama el método que escribe aumentoCienaniosGlobal */
                        aumentocien(salida);
                        break;
                    case "Fin":
                        /* Se llama el método que escribe fin */
                        cerrar(entrada,salida,cienteConectado);
                        break;

                    //En caso de no ser ninguno
                    default:
                        EnviarError(salida);
                }

            }
        } catch (IOException e) {

        }

    }

    /**
     * Método que cierrar la entrada , salida y clienteconexion
     * @param entrada
     * @param salida
     * @param cienteConectado
     * @throws IOException
     */
    private void cerrar(DataInputStream entrada, PrintStream salida, Socket cienteConectado) throws IOException {

        entrada.close();
        salida.close();
        cienteConectado.close();
    }

    /**
     * Método que retorna al cliente mayor suiza
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void mayorSuiza(PrintStream salida) throws FileNotFoundException {


        String  data ;
        String [] nivel;
        String [] nivelAnio;
        String codigoS;
        double maximo=0;
        String codigoAnio;
        double maximoAnio =0;



        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = null;

        lector = new Scanner(fichero);

        while ( lector.hasNextLine()) {
            data = lector.nextLine();


            nivel = data.split(",");
            codigoS = nivel[3];
            nivelAnio = data.split(",");
            codigoAnio= (nivelAnio[0]);

            Double numero3=Double.valueOf(codigoAnio);
            Double numero2= Double.valueOf(codigoS);

            maximo=Double.MAX_VALUE;
            maximoAnio=Double.MAX_VALUE;

            if(numero2<maximo && numero3<maximoAnio){
                maximo=numero2;
                maximoAnio=numero3;

            }

        }
        salida.println("El maximo  en Suiza es "+maximo +" en el anio"+maximoAnio);




    }
    /**
     * Método que retorna al cliente mayor global
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void mayorGlobal(PrintStream salida) throws FileNotFoundException {


        String  data ;
        String [] nivel;
        String [] nivelAnioG;
        String codigoG;
        double maximo=0;
        String codigoAnio;
        double maximoAnio =0;



        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivel = data.split(",");
            nivelAnioG = data.split(",");
            codigoAnio= nivelAnioG[0];
            codigoG = nivel[1];

            Double numero3=Double.valueOf(codigoAnio);
            Double numero2= Double.valueOf(codigoG);

            maximo=Double.MAX_VALUE;
            maximoAnio=Double.MAX_VALUE;

            if(numero2<maximo && numero3<maximoAnio){
                maximo=numero2;
                maximoAnio=numero3;

            }

        }
        salida.println("El maximo  global es"+maximo +" del anio"+maximoAnio);




    }
    /**
     * Método que retorna al cliente media suiza
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void mediaSuiza(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelSuiza;
        String codigoSuiza;
        double contador=0;
        double media = 0;
        double media2000=0;
        double media3000=0;
        double sumatotal1000 = 0;
        double sumatotal2000 = 0;
        double sumatotal3000 = 0;






        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelSuiza = data.split(",");
            codigoSuiza = nivelSuiza[3];


            contador++;
            if(contador>0 || contador==1000){
                sumatotal1000=sumatotal1000+Double.parseDouble(codigoSuiza);
                media=sumatotal1000/1000;
            }
            if(contador>1000 || contador==2000){
                sumatotal2000= sumatotal2000+Double.parseDouble(codigoSuiza);
                media2000=sumatotal2000/1000;
            }
            if(contador>2000 || contador==2015){
                sumatotal3000= sumatotal3000+Double.parseDouble(codigoSuiza);
                media3000=sumatotal3000/14;
            }





        }
        salida.println("Primer milenio  Suiza : media:"+media+" * "+"Segundo milenio Suiza : media:"+media2000+" * "+"Tercer milenio Suiza : media:"+media3000);




    }
    /**
     * Método que retorna al cliente media global
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void mediaGlobal(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelGlobal;
        String codigoGlobal;
        double contador=0;
        double mediaGlobal = 0;
        double mediaGlobal2000=0;
        double mediaGlobal3000=0;
        double sumatotalGlobal1000 = 0;
        double sumatotalGlobal2000 = 0;
        double sumatotalGlobal3000 = 0;






        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelGlobal = data.split(",");
            codigoGlobal = nivelGlobal[1];


            contador++;
            if(contador>0 || contador==1000){
                sumatotalGlobal1000=sumatotalGlobal1000+Double.parseDouble(codigoGlobal);
                mediaGlobal=sumatotalGlobal1000/1000;
            }
            if(contador>1000 || contador==2000){
                sumatotalGlobal2000= sumatotalGlobal2000+Double.parseDouble(codigoGlobal);
                mediaGlobal2000=sumatotalGlobal2000/1000;
            }
            if(contador>2000 || contador==2015){
                sumatotalGlobal3000= sumatotalGlobal3000+Double.parseDouble(codigoGlobal);
                mediaGlobal3000=sumatotalGlobal3000/14;
            }





        }
        salida.println("Primer milenio  Global : media:"+mediaGlobal+" * "+"Segundo milenio Global : media:"+mediaGlobal2000+" * "+"Tercer milenio Global : media:"+mediaGlobal3000);



    }
    /**
     * Método que retorna al cliente aumento suiza
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void aumentoSuiza(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelSuiza;
        String codigoSuiza;
        String codigoAnio;
        String []nivelAnio;
        double contador=0;
        double porecentajeSuiza =0;
        double codigo2000=0;
        double codigo2014 = 0;








        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelSuiza = data.split(",");
            nivelAnio=data.split(",");
            codigoSuiza = nivelSuiza[3];
            codigoAnio=nivelSuiza[0];

            contador++;
            if(codigoAnio.equals("2000")){

                codigo2000=Double.parseDouble(codigoSuiza);
            }
            if(codigoAnio.equals("2014")){

                codigo2014=Double.parseDouble(codigoSuiza);
            }

            porecentajeSuiza=((codigo2014-codigo2000)/codigo2000)*100;




        }
        salida.println("Aumeto Suiza"+porecentajeSuiza);





    }

    /**
     * Método que retorna al cliente aumento global
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void aumentoGlobal(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelGlobal;
        String codigoGlobal;
        String codigoAnio;
        String []nivelAnio;
        double contador=0;
        double porecentajeGlobal =0;
        double codigo2000=0;
        double codigo2014 = 0;








        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelGlobal = data.split(",");
            nivelAnio=data.split(",");
            codigoGlobal = nivelGlobal[1];
            codigoAnio=nivelAnio[0];

            contador++;
            if(codigoAnio.equals("2000")){

                codigo2000=Double.parseDouble(codigoGlobal);
            }
            if(codigoAnio.equals("2014")){

                codigo2014=Double.parseDouble(codigoGlobal);
            }

            porecentajeGlobal=((codigo2014-codigo2000)/codigo2000)*100;




        }
        salida.println(" Aumeto Suiza"+porecentajeGlobal);

    }
    /**
     * Método que retorna al cliente aumento cien años suiza
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void aumentocienaSuiza(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelSuiza;
        String codigoSuiza;
        String codigoAnio;
        String []nivelAnio;
        double contador=0;
        double porecentajeSuiza2 =0;
        double codigo1914=0;
        double codigo2014= 0;








        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelSuiza = data.split(",");
            nivelAnio=data.split(",");
            codigoSuiza = nivelSuiza[3];
            codigoAnio=nivelSuiza[0];

            contador++;
            if(codigoAnio.equals("1914")){

                codigo1914=Double.parseDouble(codigoSuiza);
            }
            if(codigoAnio.equals("2014")){

                codigo2014=Double.parseDouble(codigoSuiza);
            }

            porecentajeSuiza2=((codigo2014-codigo1914)/codigo1914)*100;




        }
        salida.println("Aumeto Suiza en los ultimos 100 anios : "+porecentajeSuiza2);






    }
    /**
     * Método que retorna al cliente aumento cien años
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void aumentocien(PrintStream salida) throws FileNotFoundException {

        String  data ;
        String [] nivelGlobal;
        String codigoGlobal;
        String codigoAnio;
        String []nivelAnio;
        double contador=0;
        double porecentajeGlobal2 =0;
        double codigo1914=0;
        double codigo2014= 0;








        File fichero =new File("carbon_dioxide_in_air.csv");

        Scanner lector = new Scanner(fichero);

        while (lector.hasNextLine()) {
            data = lector.nextLine();


            nivelGlobal = data.split(",");
            nivelAnio=data.split(",");
            codigoGlobal = nivelGlobal[1];
            codigoAnio=nivelAnio[0];

            contador++;
            if(codigoAnio.equals("1914")){

                codigo1914=Double.parseDouble(codigoGlobal);
            }
            if(codigoAnio.equals("2014")){

                codigo2014=Double.parseDouble(codigoGlobal);
            }

            porecentajeGlobal2=((codigo2014-codigo1914)/codigo1914)*100;




        }
        salida.println("Aumeto global en los ultimos 100 anios :"+porecentajeGlobal2);


    }




    /**
     * Método que retorna al cliente un mensaje en caso de error
     *
     * @param salida PrintStream que representa el canal de escritura por socket
     *               del servidor al cliente
     */
    private void EnviarError(PrintStream salida) {
        salida.println("Error");
    }

    /**
     * Método que  conecta que arranca el servidor
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //Se arranca el servidor
        Servidor s = new Servidor(4444);
    }
}
