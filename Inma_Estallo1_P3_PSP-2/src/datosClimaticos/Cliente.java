package datosClimaticos;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class Cliente {

    private int puerto;
    private String host;
    private Socket socket;

    /**
     * Constructor al que se le pasa el parametro host y puerto
     * @param host
     * @param puerto
     */
    public Cliente(String host, int puerto) {
        this.puerto = puerto;
        this.host = host;
    }

    /**
     * Método en le que recibes la información del servidor
     * @throws IOException
     */
    public void conectar() throws IOException {

        socket = new Socket(host, puerto);

        // Declaramos un DataInputStream para leer los datos
        DataInputStream entrada = new DataInputStream(socket.getInputStream());

        // Declaramos un DataOutputStream para escribir los datos
        PrintStream salida = new PrintStream(socket.getOutputStream());

        // Creamos un String para la informacion recibida
        String recibida;

        salida.println("Mayor cantidad Suiza");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("Mayor Cantidad global");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("media suiza");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("media global");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("aumento suiza");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("aumentoglobal");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("aumentocienaniosSuiza");
        recibida = entrada.readLine();
        System.out.println(recibida);

        salida.println("aumentocienanios");
        recibida = entrada.readLine();
        System.out.println(recibida);


        salida.println("Fin");
        recibida = entrada.readLine();

        //Se cierra en canal de entrada
        entrada.close();
        //Se cierra el canal de salida
        salida.close();
        //Se cierra el socket
        socket.close();
    }

    /**
     * Método con el que conectas al servidor
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //Se crea un nuevo cliente
        Cliente c = new Cliente("localhost",4444);
        //Se conecta al servidor
        c.conectar();
    }

}
