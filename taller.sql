CREATE DATABASE IF NOT EXISTS tallermecanico;
USE tallermecanico;

CREATE TABLE IF NOT EXISTS taller
(
	id iNT PRIMARY KEY AUTO_INCREMENT,
	nombretaller VARCHAR(50),
	fecha_creacion date,
	telefono VARCHAR(50),
    numero_factura int
  
	
);
CREATE TABLE IF NOT EXISTS mecanico
(
	id INT  PRIMARY KEY AUTO_INCREMENT,
	nombremecanico VARCHAR(10) ,
    apellido VARCHAR(10) ,
	fecha_nacimiento date,
	aniosexperiencia int
);

CREATE TABLE IF NOT EXISTS coche
(
	id INT  PRIMARY KEY AUTO_INCREMENT,
	modelo VARCHAR(10) ,
    anioscoche int,
    fecha_creacion date
	
);
CREATE TABLE IF NOT EXISTS cliente
(
	id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(10) ,
    fecha_nacimiento date,
	anioscoche int
	
);

CREATE TABLE IF NOT EXISTS taller_mecanico
(
	id_taller INT UNSIGNED REFERENCES taller,
	id_mecanico INT UNSIGNED REFERENCES mecanico,
	PRIMARY KEY (id_taller, id_mecanico)
);
CREATE TABLE IF NOT EXISTS mecanico_coche
(
	id_mecanico INT UNSIGNED REFERENCES mecanico,
	id_coche INT UNSIGNED REFERENCES coche,
	PRIMARY KEY (id_mecanico, id_coche)
);
CREATE TABLE IF NOT EXISTS coche_cliente
(
	id_coche INT UNSIGNED REFERENCES coche,
	id_cliente INT UNSIGNED REFERENCES cliente,
	PRIMARY KEY (id_coche ,id_cliente)
);